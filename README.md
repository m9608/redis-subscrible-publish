**Redis 发布、订阅 demo实践**



redis消息队列的几种实现方式

- PubSub
- Stream 
- 基于List的Lpush+BRPOP的实现
- 基于Sorted-Set的实现

> 四种方式的介绍
>
> https://blog.csdn.net/weixin_40918067/article/details/116582463
>
> 上面这个连接的帖子对stream的介绍可以仔细看看



# 项目结构



# Redis sub/pub

**优点**

典型的广播模式，一个消息可以发布到多个消费者

多信道订阅，消费者可以同时订阅多个信道，从而接收多类消息

消息即时发送，消息不用等待消费者读取，消费者会自动接收到信道发布的消息

**缺点**

消息一旦发布，不能接收。换句话就是发布时若客户端不在线，则消息丢失，不能寻回

不能保证每个消费者接收的时间是一致的

若消费者客户端出现消息积压，到一定程度，会被强制断开，导致消息意外丢失。通常发生在消息的生产远大于消费速度时

可见，Pub/Sub 模式不适合做消息存储，消息积压类的业务，而是擅长处理广播，即时通讯，即时反馈的业务。











## RedisTemplate配置

如果不定义 RedisAutoCOnfiguration类会自动注册一个 RedisTemplate<Object, Object> 类型的bean,并且在存在多个泛型类型的时候，想要注入自己创建的，就需要把泛型类型带上。

如果注入不进去，在多模块的结构时，请检查@ComponentScan注解是否将配置类的路径给带进去了。

```java
public class RedisConfig {

    @Bean(name = "redisTemplate")
    @SuppressWarnings("all")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(redisConnectionFactory);
        RedisSerializer<?> serializer = RedisConfigHolder.getRedisSerializer();

        template.setDefaultSerializer(serializer);

        // 使用StringRedisSerializer来序列化和反序列化redis的key值
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(serializer);

        // Hash的key也采用StringRedisSerializer的序列化方式
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(serializer);
        template.afterPropertiesSet();
        return template;
    }
}
```



## Redis的序列化配置，autoType配置

如果设置autoType为true,那么在序列化为字符串的时候，会加一个@Type参数，该参数是java类的类名，如果是多个跨平台的服务则不建议这么做类名保持一致是不现实的。

```java
public class FastJson2JsonRedisSerializer<T> implements RedisSerializer<T> {
    static {
        ParserConfig.getGlobalInstance().setAutoTypeSupport(false);
    }
}
```



## Redis Publisher

直接调用此方法即可

```java
 redisTemplate.convertAndSend(RedisChannelConstants.TOPIC_A, new MessageDto("a", "b", "c"));
```



## Redis Subcribler

### 1、消息监听容器 RedisMessageListenerContainer 

通过addMessageListener方法将消息监听器给注册进去，有两种方法

- 实现接口 MessageListener
- 组装 MessageListenerAdapter

**目前实验的结论是，无法在运行时注册新的监听器，只能在程序启动时注册bean的时候加进去。**

```java
@Configuration
public class RedisSubscribleConfig {

    /**
     * Redis消息监听器容器 这个容器加载了RedisConnectionFactory和消息监听器 可以添加多个监听不同话题的redis监听器，只需要把消息监听器和相应的消息订阅处理器绑定，该消息监听器
     * 通过反射技术调用消息订阅处理器的相关方法进行一些业务处理
     *
     * @param redisConnectionFactory 连接工厂
     * @param adapter                适配器
     * @return redis消息监听容器
     */
    @Bean
    @SuppressWarnings("all")
    public RedisMessageListenerContainer container(RedisConnectionFactory redisConnectionFactory,
            RedisMessageListener listener, RedisTemplate redisTemplate) {

        RedisSerializer seria = RedisConfigHolder.getRedisSerializer();

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        // 监听所有库的key过期事件
        container.setConnectionFactory(redisConnectionFactory);
        // 所有的订阅消息，都需要在这里进行注册绑定,new PatternTopic(TOPIC_NAME1)表示发布的主题信息
        // 可以添加多个 messageListener，配置不同的通道
        container.addMessageListener(listener, new PatternTopic(RedisChannelConstants.TOPIC_A));

        MessageListenerAdapter receiveMessage = MessageListenerAdapterFactory.create(new MessageSubscriber());
        container.addMessageListener(receiveMessage,
                new PatternTopic(RedisChannelConstants.TOPIC_B));

        /**
         * 设置序列化对象
         * 特别注意：1. 发布的时候需要设置序列化；订阅方也需要设置序列化
         *         2. 设置序列化对象必须放在[加入消息监听器]这一步后面，否则会导致接收器接收不到消息
         */
        container.setTopicSerializer(seria);

        return container;
    }
}
```



### 2、实现接口 MessageListener

```java
@Component
@Slf4j
public class RedisMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message, byte[] pattern) {
        MessageDto messageDto = JSON.parseObject(message.toString(),MessageDto.class);
        log.info("RedisMessageListener->onMessage:{}, pattern:{}", messageDto, new String(pattern));
    }
}
```

### 3、组装 MessageListenerAdapter

MessageListenerAdapter 需要注意，他需要制定一个方法名来接收参数，这个方法的参数列表只能有两种类型

Object messageDto 已经是通过 反序列化将Message message对象的body反序列化出来的结果，只不过由于redisTemplat序列化的配置，他转换成的类型是对应的配置产生的类型比如 JSONOBJECT。这里只能用Object来保证接收。这里比较坑的是，只要参数类型匹配不上就不会执行方法。在本项目里通过泛型进行了封装，使得使用者可以定义消息对应的类型。

```java
1、(Object messageDto, @Nullable String pattern);
2、(String pattern);
```

### 4、MessageListenerAdapter的参数是否能够指定成类型？

可以，**需要看你使用的序列化是什么类型的**

如果序列化后，你的json对象中携带了@Type参数，并且在目标订阅者服务中根据该属性能够找到类型名，那么就可以直接将其转换为该类的对象，这样通过方法参数进行查找对应的方法时，就能够匹配的上。因此对对象的class有着强依赖，因此**不建议使用这种方式**。

### 5、消息订阅类型

可以通过指定的名字进行订阅，也可以通过指定的匹配模式进行批量订阅

以上是能够简单实现消息订阅发布的核心内容实践。

------



redission订阅者，如果使用的是jackJson的话，并且两边使用的类路径一样，那么在消息监听器上面设置的泛型就会起作用，如果使用fastJson转成不带类型的json序列化格式，那么消息监听器上面的泛型就会失效，还是只能使用Object进行转换



更完善的方案

新增订阅

取消订阅，正则匹配批量取消订阅

需要处理的，异常情况下需要进行处理的回调。



有空的话可以看看为什么不能在运行时添加订阅





# Redis Stream

Redis Stream 是 **Redis 5.0** 版本新增加的数据结构。

看过的文章

> Redis Stream的使用
>
> https://blog.csdn.net/qq_41993206/article/details/112740950

消息发布订阅模式，每一个订阅者都能够拿到所有发布的消息，这样在分布式微服务的情况下，消费者服务会有多个，无法指定某一个消息只能被消费一次。同时消息无法持久化，如果出现网络断开，redis宕机等情况时，消息就会被丢弃。此时就可以考虑使用redis stream

Stream的消费模型借鉴了kafka的消费分组的概念，它弥补了Redis Pub/Sub不能持久化消息的缺陷。

## 特点

- 消息ID的序列化生成
- 消息遍历
- 消息的阻塞和非阻塞读取
- 消息的分组消费
- 未完成消息的处理
- 消息队列监控



## Stream的结构



- 每个stream都有一个唯一的名称就是Redis的key，在首次使用xadd指令追加消息时自动创建。

- 每个stream可以有多个消费组**Consumer Group**，使用 XGROUP CREATE 命令创建，一个消费组有多个消费者(Consumer)，他们之间是竞争者关系，每个消费者有一个组内唯一名称。

- 每个消费组会有一个游标**last_delivered_id**，组内任意一个消费者读取了消息都会使游标 last_delivered_id 往前移动。

- **pending_ids**【PEL】 ：消费者的状态变量，作用是维护消费者未确认的id。pending_ids记录了当前已经被客户端读取的信息，但是还没有ack。它用来确保客户端至少消费了消息一次，而不会再网络传输的中途丢失了没处理。读到新消息后，对应的消息ID就会进入消费者的PEL(正在处理的消息)结构里，客户端处理完毕后使用xack指令通知服务器，本条消息已经处理完毕，该消息ID就会从PEL中移除。

  

  

## stream相关命令

**消息队列相关命令**

- **XADD** - 添加消息到末尾
- **XTRIM** - 对流进行修剪，限制长度
- **XDEL** - 删除消息
- **XLEN** - 获取流包含的元素数量，即消息长度
- **XRANGE** - 获取消息列表，会自动过滤已经删除的消息
- **XREVRANGE** - 反向获取消息列表，ID 从大到小
- **XREAD** - 以阻塞或非阻塞方式获取消息列表

**消费者组相关命令：**

- **XGROUP CREATE** - 创建消费者组
- **XREADGROUP GROUP** - 读取消费者组中的消息
- **XACK** - 将消息标记为"已处理"
- **XGROUP SETID** - 为消费者组设置新的最后递送消息ID
- **XGROUP DELCONSUMER** - 删除消费者
- **XGROUP DESTROY** - 删除消费者组
- **XPENDING** - 显示待处理消息的相关信息
- **XCLAIM** - 转移消息的归属权
- **XINFO** - 查看流和消费者组的相关信息；
- **XINFO GROUPS** - 打印消费者组的信息；
- **XINFO STREAM** - 打印流信息

## 涉及的问题

1、消息如果忘记ACK

​	PEL就会一直增大

2、PEL如何避免消息丢失

**丢失的时间点：**在客户端消费者读取Stream消息时，Redis服务器将消息回复给客户端的过程中，客户端突然断开了连接，消息就丢失了。

但是PEL里已经保存了发出去的消息ID。

待客户端重新连上之后，可以再次收到PEL中的消息ID列表。

不过此时xreadgroup的起始消息必须是任意有效的消息ID，一般将参数设为0-0，表示读取所有的PEL消息以及自last_delivered_id之后的新消息。

3、Stream消息太多怎么办?设置上限







































# Redis List

List支持多个生产者和消费者并发进出信息，**每个消费者拿到的都是不同的列表元素**【意味着适合消费者是多台机器分布式的架构】

使用rpush和lpush操作入队列，lpop和rpop操作出队列

但是当队列为空时，lpop和rpop会一直空轮训，消耗资源，所以引入阻塞读blpop和brpop，阻塞读在队列没有数据的时候进入休眠状态，一旦数据到来则立刻醒过来，消息延迟几乎为零。

存在的问题：

空闲连接。如果线程一直阻塞，redis连接就成了闲置连接，时间过长后，服务器会主动断开连接，减少资源占用，这时blpop和brpop可能会抛出异常。

所以编写客户端消费者时，要捕捉异常，还有重试

缺点：

不能做广播模式

不能重复消费，一旦消费就会被删除

不支持分组消费

做消费者确认ACK比较麻烦，不能保证消费者消费信息后是否成功处理的问题。（宕机或者消费异常）





# 基于Sorted-Set的实现



Sortes Set(有序列表)，类似于java的SortedSet和HashMap的结合体，一方面她是一个set，保证内部value的唯一性，另一方面它可以给每个value赋予一个score，代表这个value的

排序权重。内部实现是“跳跃表”。

有序集合的方案是在自己确定消息顺ID时比较常用，使用集合成员的Score来作为消息ID，保证顺序，还可以保证消息ID的单调递增。通常可以使用时间戳+序号的方案。确保了消息ID的单调递增，利用SortedSet的依据

Score排序的特征，就可以制作一个有序的消息队列了。

**优点**

就是可以自定义消息ID，在消息ID有意义时，比较重要。

**缺点**

缺点也明显，不允许重复消息（因为是集合），同时消息ID确定有错误会导致消息的顺序出错。











































