package com.twill.redis.publisher.service;

import com.twill.redis.common.RedisChannelConstants;
import com.twill.redis.common.dto.MessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


/**
 * @author will.tuo
 * @date 2023/3/29 10:57
 * @description 无
 */
@Service("publisherService")
public class PublisherService {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public void publish() {
        redisTemplate.convertAndSend(RedisChannelConstants.TOPIC_A, new MessageDto("a", "b", "c"));
    }

    public void publish2() {
        redisTemplate.convertAndSend(RedisChannelConstants.TOPIC_B, new MessageDto("a", "b", "c"));
    }
}
