package com.twill.redis.publisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.twill.redis")
public class RedisPublisherApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisPublisherApplication.class, args);
    }

}
