package com.twill.redis.publisher.controller;

import com.twill.redis.publisher.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisController {

    @Autowired
    PublisherService publisherService;

    @GetMapping("/redis")
    public Object redisTest() {
        publisherService.publish();
        publisherService.publish2();
        return "success";
    }
}
