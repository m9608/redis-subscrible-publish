package com.twill.redis.common;

/**
 * @author will.tuo
 * @date 2023/3/29 14:10
 * @description 无
 */
public class RedisChannelConstants {

    public static final String TOPIC_A = "Topic_A";
    public static final String TOPIC_B = "Topic_B";

}
