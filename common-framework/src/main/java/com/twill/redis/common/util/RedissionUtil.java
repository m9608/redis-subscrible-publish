package com.twill.redis.common.util;

import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2022/5/23 15:32
 */
@Service
@Slf4j
public class RedissionUtil {

    @Autowired
    private RedissonClient redissonClient;

    public <T> T getBucket(String key) {
        if (key == null || key.isEmpty()) {
            return null;
        }
        try {
            RBucket<T> bucket = redissonClient.getBucket(key);
            if (bucket != null) {
                return bucket.get();
            }
        } catch (Exception e) {
            log.error("[RedisComponent] redis get error key:{}", key, e);
        }
        return null;
    }

    public boolean set(String key, Object data) {
        if (key == null || key.isEmpty()) {
            return false;
        }
        try {
            redissonClient.getBucket(key).set(data);
        } catch (Exception e) {
            log.error("[RedisComponent] redis set error key:{} ", key, e);
            return false;
        }
        return true;
    }

    public boolean set(String key, Object data, long timeToLive, TimeUnit timeUnit) {
        if (key == null || key.isEmpty()) {
            return false;
        }
        try {
            redissonClient.getBucket(key).set(data, timeToLive, timeUnit);
        } catch (Exception e) {
            log.error("[RedisComponent] redis set error key:{} ", key, e);
            return false;
        }
        return true;
    }


}
