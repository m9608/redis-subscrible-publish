package com.twill.redis.common.config;

import com.alibaba.fastjson.support.spring.FastJsonRedisSerializer;
import com.twill.redis.common.dto.MessageDto;
import lombok.Getter;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @author will.tuo
 * @date 2023/3/29 17:22
 * @description 无
 */
@Getter
public class RedisConfigHolder {


    private final static RedisSerializer<Object> redisSerializer;

    private final static FastJsonCodec fastJsonCodec;

    static {
        //        ObjectMapper mapper = new ObjectMapper();
//        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        mapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL,
//                JsonTypeInfo.As.PROPERTY);
//        fastJson2JsonRedisSerializer.setObjectMapper(mapper);

        redisSerializer = new FastJsonRedisSerializer<>(Object.class);
        fastJsonCodec = new FastJsonCodec();
    }

    public static FastJsonCodec getFastJsonCodec() {
        return fastJsonCodec;
    }

    public static RedisSerializer<?> getRedisSerializer() {
        return redisSerializer;
    }

    public static void main(String[] args) {
        MessageDto messageDto = new MessageDto("a", "b", "c");
        byte[] serialize = redisSerializer.serialize(messageDto);
        Object deserialize = redisSerializer.deserialize(serialize);
        System.out.println(deserialize);

    }

}
