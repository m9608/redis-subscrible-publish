package com.twill.redis.common.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public interface MethodWrapper {

    Object invoke() throws InvocationTargetException, IllegalAccessException;

    static MethodWrapper wrapMethod(Object beanObj, Method method) {
        return new IServiceMethodWrapper(beanObj, method);
    }

    static MethodWrapper wrapMethodWithParameters(Object beanObj, Method method, Object... args) {
        return new IServiceMethodWithParametersWrapper(beanObj, method, args);
    }

    class IServiceMethodWrapper implements MethodWrapper {

        private final Object beanObj;
        private final Method method;

        private IServiceMethodWrapper(@NonNull Object beanObj, @NonNull Method method) {
            this.method = method;
            this.beanObj = beanObj;
        }

        @Override
        public Object invoke() throws InvocationTargetException, IllegalAccessException {
            return method.invoke(beanObj);
        }
    }

    class IServiceMethodWithParametersWrapper implements MethodWrapper {

        private final Object beanObj;
        private final Method method;

        private final Object[] args;

        private IServiceMethodWithParametersWrapper(@NonNull Object beanObj, @NonNull Method method,
                @Nullable Object... args) {
            this.method = method;
            this.beanObj = beanObj;
            this.args = args;
        }

        @Override
        public Object invoke() throws InvocationTargetException, IllegalAccessException {
            return method.invoke(beanObj, args);
        }
    }
}
