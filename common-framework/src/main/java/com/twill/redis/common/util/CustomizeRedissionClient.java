package com.twill.redis.common.util;

import com.twill.redis.common.config.RedisConfigHolder;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 * @author will.tuo
 * @date 2023/3/30 10:08
 * @description 无
 */
public class CustomizeRedissionClient {

    private static final RedissonClient redissonClient;

    static {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://localhost:6379");
        // 消息发送者与订阅者的编码方式保持一致，否则无法将信息解码报错
        config.setCodec(RedisConfigHolder.getFastJsonCodec());
        redissonClient = Redisson.create(config);
    }

    public static RedissonClient get() {
        return redissonClient;
    }
}
