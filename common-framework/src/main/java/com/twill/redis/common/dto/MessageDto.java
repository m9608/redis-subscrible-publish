package com.twill.redis.common.dto;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author will.tuo
 * @date 2023/3/29 13:51
 * @description 无
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageDto implements Serializable {

    private String data = "data";
    private String title = "title";
    private String content = "content";
}
