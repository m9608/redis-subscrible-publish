package com.twill.redis.common.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

@Component
public class IServiceReflectionUtils extends SpringBeanUtils {

    public static Object invokeMethod(String serviceName, String methodName)
            throws InvocationTargetException, IllegalAccessException {
        return getServiceMethod(serviceName, methodName).invoke();
    }

    public static Object invokeMethod(String serviceName, String methodName, @Nullable Object... args)
            throws InvocationTargetException, IllegalAccessException {
        return getServiceMethod(serviceName, methodName, args).invoke();
    }

    public static MethodWrapper getServiceMethod(String serviceName, String methodName) {
        return getServiceMethod(serviceName, methodName, new Object[0]);
    }

    public static MethodWrapper getServiceMethod(String serviceName, String methodName, @Nullable Object... args) {
        Object beanObj = getBean(serviceName);
        if (beanObj == null) {
            throw new IllegalArgumentException("Not exist service:" + serviceName);
        }
        List<Class<?>> paramTypeList = new ArrayList<>();
        if (args != null && args.length > 0) {
            for (Object arg : args) {
                paramTypeList.add(arg.getClass());
            }
        }
        Method method = ReflectionUtils.findMethod(beanObj.getClass(), methodName,
                paramTypeList.toArray(new Class[]{}));
        if (method == null) {
            throw new IllegalArgumentException("Not exist serviceMethod:" + serviceName + "." + methodName);
        }
        if (args == null) {
            return MethodWrapper.wrapMethod(beanObj, method);
        }
        return MethodWrapper.wrapMethodWithParameters(beanObj, method, args);
    }
}
