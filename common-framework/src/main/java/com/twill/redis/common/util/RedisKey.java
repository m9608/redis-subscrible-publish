package com.twill.redis.common.util;

/**
 * @author will.tuo
 * @date 2022/5/24 16:30
 */
public enum RedisKey {
    /**
     * 自定义的key格式
     */
    CLASS_ID_CACHE("%s|%d", "class.getTypeName|id");

    RedisKey(String format, String description) {
        this.format = format;
        this.description = description;
    }

    private final String format;
    private final String description;

    public String getKey(String className, long id) {
        return String.format(this.format, className, id);
    }

    public String getDescription(String className, long id) {
        return String.format(this.description, className, id);
    }

}
