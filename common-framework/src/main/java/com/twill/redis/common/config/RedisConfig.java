package com.twill.redis.common.config;

import com.twill.redis.common.dto.MessageDto;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author will.tuo
 * @date 2021/8/9 9:48
 * <p>
 * 通过查看 RedisAutoConfiguration源码 SpringBoot自动帮我们在容器中生成了一个RedisTemplate和一个StringRedisTemplate。
 * 但是，这个RedisTemplate的泛型是<Object,Object>， 写代码不方便，需要写好多类型转换的代码； 我们需要一个泛型为<String,Object>形式的RedisTemplate。
 * 并且，这个RedisTemplate没有设置数据存在Redis时，key及value的序列化方式。 看到这个@ConditionalOnMissingBean注解后， 就知道如果Spring容器中有了RedisTemplate对象了，
 * 这个自动配置的RedisTemplate不会实例化。 因此我们可以直接自己写个配置类，配置RedisTemplate。
 */
@Configuration
public class RedisConfig {

    @Bean(name = "redisTemplate")
    @SuppressWarnings("all")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(redisConnectionFactory);
        RedisSerializer<?> serializer = RedisConfigHolder.getRedisSerializer();

        template.setDefaultSerializer(serializer);

        // 使用StringRedisSerializer来序列化和反序列化redis的key值
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(serializer);

        // Hash的key也采用StringRedisSerializer的序列化方式
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(serializer);
        template.afterPropertiesSet();
        return template;
    }
}
