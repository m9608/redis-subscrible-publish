package com.twill.redission.puber.controller;

import com.twill.redis.common.RedisChannelConstants;
import com.twill.redis.common.dto.MessageDto;
import com.twill.redis.common.util.CustomizeRedissionClient;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2023/3/30 10:02
 * @description 经测试，这两种发布方式都可以生效
 */
@RestController
public class PuberController {

    @Autowired
    private RedissonClient redissonClient;

    @GetMapping("/pub")
    public Object pub() {

        RedissonClient redissonClient1 = CustomizeRedissionClient.get();
        RTopic topic = redissonClient1.getTopic(RedisChannelConstants.TOPIC_A);
        topic.publish(new MessageDto());
        return "success";
    }

    @GetMapping("/pub1")
    public Object pub1() {
        RTopic topic = redissonClient.getTopic(RedisChannelConstants.TOPIC_A);
        topic.publish(new MessageDto());
        topic.removeListener();
        return "success";
    }

    @GetMapping("/count")
    public Object count() {
        RTopic topic = redissonClient.getTopic(RedisChannelConstants.TOPIC_A);
        topic.publish(new MessageDto());
        return topic.countListeners() + "|" + topic.countSubscribers();
    }


}
