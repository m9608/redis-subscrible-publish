package com.twill.redission.suber.controller;

import com.twill.redis.common.RedisChannelConstants;
import com.twill.redis.common.util.CustomizeRedissionClient;
import com.twill.redission.suber.service.RedissionML;
import java.util.concurrent.ExecutionException;
import org.redisson.api.RFuture;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2023/3/30 10:14
 * @description 经测试，运行期间的添加和删除都可以生效
 */
@RestController
public class SubController {

    private static Integer listenerId;


    @GetMapping("/addSub")
    public Object sub() throws ExecutionException, InterruptedException {
        RedissonClient redissonClient = CustomizeRedissionClient.get();
        RTopic topic = redissonClient.getTopic(RedisChannelConstants.TOPIC_A);
        RFuture<Integer> integerRFuture = topic.addListenerAsync(Object.class, new RedissionML());
        listenerId = integerRFuture.get();
        return "success";
    }

    @GetMapping("/reSub")
    public Object remove() {
        RedissonClient redissonClient = CustomizeRedissionClient.get();
        RTopic topic = redissonClient.getTopic(RedisChannelConstants.TOPIC_A);
        topic.removeListenerAsync(listenerId);
        return "success";
    }
}
