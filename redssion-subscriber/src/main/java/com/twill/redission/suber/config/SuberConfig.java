package com.twill.redission.suber.config;

import com.twill.redis.common.RedisChannelConstants;
import com.twill.redis.common.util.CustomizeRedissionClient;
import com.twill.redission.suber.service.RedissionML;
import javax.annotation.PostConstruct;
import org.redisson.api.RFuture;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2023/3/30 15:50
 * @description 无
 */
@Service
public class SuberConfig {

    @PostConstruct
    public void init() {
        RedissonClient redissonClient = CustomizeRedissionClient.get();
        RTopic topic = redissonClient.getTopic(RedisChannelConstants.TOPIC_A);
        RFuture<Integer> integerRFuture = topic.addListenerAsync(Object.class, new RedissionML());
    }

}
