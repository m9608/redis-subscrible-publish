package com.twill.redission.suber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.twill.redis", "com.twill.redission"})
public class RedissionSuberApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedissionSuberApplication.class, args);
    }

}
