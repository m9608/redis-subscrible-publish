package com.twill.redission.suber.service;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.listener.MessageListener;

/**
 * @author will.tuo
 * @date 2023/3/30 10:13
 * @description 无
 */
@Slf4j
public class RedissionML implements MessageListener<Object> {

    @Override
    public void onMessage(CharSequence charSequence, Object messageDto) {
        log.info("RedissionMessageListener->onMessage:{}, messageDto:{}", charSequence, messageDto);

    }
}
