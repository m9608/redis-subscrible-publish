package com.twill.redis.subscrible.config;

import com.twill.redis.common.dto.MessageDto;
import com.twill.redis.subscrible.subscriber.Subscriber;
import lombok.extern.slf4j.Slf4j;

/**
 * @author will.tuo
 * @date 2023/3/29 14:05
 * @description 无
 */
@Slf4j
public class MessageSubscriber implements Subscriber<MessageDto> {

    @Override
    public void onMessage(MessageDto messageDto, String pattern) {
        log.info("MessageSubscriber->receiveMessage:{}, pattern:{}", messageDto, pattern);
    }

    @Override
    public Class<MessageDto> getType() {
        return MessageDto.class;
    }
}
