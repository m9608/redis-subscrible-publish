package com.twill.redis.subscrible.subscriber;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.twill.redis.common.dto.MessageDto;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * 将 Receiver Subscriber结合，可将泛型用于onMessage方法 {@link MessageListenerAdapter} 的 消息类型适配器
 *
 * @param <T>
 */
public class MessageTypeAdapter<T> implements Receiver, MessageListener {

    private final Subscriber<T> subscriber;
    private final Class<T> messageType;

    public MessageTypeAdapter(Subscriber<T> subscriber, Class<T> messageType) {
        this.subscriber = subscriber;
        this.messageType = messageType;
    }

    /**
     * MessageListenerAdapter 适配方法
     *
     * @param message
     * @param pattern
     */
    @Override
    @SuppressWarnings("unchecked")
    public void receiveMessage(MessageDto message, String pattern) {
        subscriber.onMessage(JSON.parseObject(JSONObject.toJSONString(message), messageType), pattern);
    }

    /**
     * MessageListener 适配方法，其实这里不需要，因为引用了MessageListener就直接走onMessage了
     *
     * @param message
     * @param pattern
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        subscriber.onMessage(JSON.parseObject(message.toString(), messageType),
                new String(pattern));
    }
}
