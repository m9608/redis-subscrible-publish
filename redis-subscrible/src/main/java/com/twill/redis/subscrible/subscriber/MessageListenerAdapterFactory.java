package com.twill.redis.subscrible.subscriber;

import com.twill.redis.common.config.RedisConfigHolder;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.RedisSerializer;

public class MessageListenerAdapterFactory {
    private static final String METHODS_NAME = "receiveMessage";

    public static  <T> MessageListenerAdapter create(Subscriber<T> subscriber) {
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter(new MessageTypeAdapter<>(subscriber, subscriber.getType()), METHODS_NAME);
        messageListenerAdapter.setSerializer(RedisConfigHolder.getRedisSerializer());
        messageListenerAdapter.afterPropertiesSet();
        return messageListenerAdapter;
    }

    public static  <T> MessageListenerAdapter create(Subscriber<T> subscriber, RedisSerializer<?> serializer) {
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter(new MessageTypeAdapter<>(subscriber, subscriber.getType()), METHODS_NAME);
        messageListenerAdapter.setSerializer(serializer);
        messageListenerAdapter.afterPropertiesSet();
        return messageListenerAdapter;
    }
}
