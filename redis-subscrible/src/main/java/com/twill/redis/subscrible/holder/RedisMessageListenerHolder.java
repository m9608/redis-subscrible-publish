package com.twill.redis.subscrible.holder;

import javax.annotation.PostConstruct;
import lombok.NonNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

/**
 * @author will.tuo
 * @date 2023/3/29 15:23
 * @description 无
 */
@Component
public class RedisMessageListenerHolder implements ApplicationContextAware {

    private static RedisMessageListenerContainer redisMessageListenerContainer = null;
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @PostConstruct
    public void init() throws BeansException {
        redisMessageListenerContainer = applicationContext.getBean(RedisMessageListenerContainer.class);
    }

    public RedisMessageListenerHolder(RedisMessageListenerContainer container) {
        redisMessageListenerContainer = container;
    }

    public static RedisMessageListenerContainer getContainer() {
        return redisMessageListenerContainer;
    }
}
