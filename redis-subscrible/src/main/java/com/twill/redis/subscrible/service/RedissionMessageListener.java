package com.twill.redis.subscrible.service;

import com.twill.redis.common.dto.MessageDto;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.listener.MessageListener;

/**
 * @author will.tuo
 * @date 2023/3/30 9:41
 * @description 无
 */
@Slf4j
public class RedissionMessageListener implements MessageListener<MessageDto> {

    @Override
    public void onMessage(CharSequence charSequence, MessageDto messageDto) {
        log.info("RedissionMessageListener->onMessage:{}, messageDto:{}", charSequence, messageDto);
    }
}
