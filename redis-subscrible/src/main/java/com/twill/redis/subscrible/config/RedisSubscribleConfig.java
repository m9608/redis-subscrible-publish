package com.twill.redis.subscrible.config;

import com.twill.redis.common.RedisChannelConstants;
import com.twill.redis.common.config.RedisConfigHolder;
import com.twill.redis.subscrible.subscriber.MessageListenerAdapterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @author will.tuo
 * @date 2023/3/29 13:58
 * @description 无
 */
@Configuration
public class RedisSubscribleConfig {

    /**
     * Redis消息监听器容器 这个容器加载了RedisConnectionFactory和消息监听器 可以添加多个监听不同话题的redis监听器，只需要把消息监听器和相应的消息订阅处理器绑定，该消息监听器
     * 通过反射技术调用消息订阅处理器的相关方法进行一些业务处理
     *
     * @param redisConnectionFactory 连接工厂
     * @param adapter                适配器
     * @return redis消息监听容器
     */
    @Bean
    @SuppressWarnings("all")
    public RedisMessageListenerContainer container(RedisConnectionFactory redisConnectionFactory,
            RedisMessageListener listener, RedisTemplate redisTemplate) {

        RedisSerializer seria = RedisConfigHolder.getRedisSerializer();

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        // 监听所有库的key过期事件
        container.setConnectionFactory(redisConnectionFactory);
        // 所有的订阅消息，都需要在这里进行注册绑定,new PatternTopic(TOPIC_NAME1)表示发布的主题信息
        // 可以添加多个 messageListener，配置不同的通道
        container.addMessageListener(listener, new PatternTopic(RedisChannelConstants.TOPIC_A));

        MessageListenerAdapter receiveMessage = MessageListenerAdapterFactory.create(new MessageSubscriber());
        container.addMessageListener(receiveMessage,
                new PatternTopic(RedisChannelConstants.TOPIC_B));

        /**
         * 设置序列化对象
         * 特别注意：1. 发布的时候需要设置序列化；订阅方也需要设置序列化
         *         2. 设置序列化对象必须放在[加入消息监听器]这一步后面，否则会导致接收器接收不到消息
         */
        container.setTopicSerializer(seria);

        return container;
    }
}
