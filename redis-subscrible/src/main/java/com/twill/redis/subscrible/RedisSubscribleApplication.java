package com.twill.redis.subscrible;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@SpringBootApplication
@EnableRedisRepositories
@ComponentScan(basePackages = "com.twill.redis")
public class RedisSubscribleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisSubscribleApplication.class, args);
    }

}
