package com.twill.redis.subscrible.subscriber;

import org.springframework.lang.Nullable;

/**
 * 用于定义具有泛型的消息订阅者
 *
 * @param <T>
 */
public interface Subscriber<T> {

    void onMessage(T messageDto, @Nullable String pattern);

    Class<T> getType();
}
