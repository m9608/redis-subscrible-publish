package com.twill.redis.subscrible.subscriber;

import com.twill.redis.common.dto.MessageDto;
import org.springframework.lang.Nullable;

/**
 * onReceive 方法名用于在定义Adapter时，定义需要回调的方法，该方法只允许有两种类型的参数 1、(Object messageDto, @Nullable String pattern); 2、(String
 * pattern);
 */
public interface Receiver {

    void receiveMessage(MessageDto messageDto, @Nullable String pattern);
}
