package com.twill.redis.subscrible.config;

import com.alibaba.fastjson.JSON;
import com.twill.redis.common.dto.MessageDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

/**
 * @author will.tuo
 * @date 2023/3/29 14:00
 * @description 无
 */
@Component
@Slf4j
public class RedisMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message, byte[] pattern) {
        MessageDto messageDto = JSON.parseObject(message.toString(), MessageDto.class);
        log.info("RedisMessageListener->onMessage:{}, pattern:{}", messageDto, new String(pattern));
    }
}
