package com.twill.redis.subscrible.controller;

import com.twill.redis.common.RedisChannelConstants;
import com.twill.redis.common.config.RedisConfigHolder;
import com.twill.redis.common.util.IServiceReflectionUtils;
import com.twill.redis.subscrible.config.MessageSubscriber;
import com.twill.redis.subscrible.config.RedisMessageListener;
import com.twill.redis.subscrible.holder.RedisMessageListenerHolder;
import java.lang.reflect.InvocationTargetException;

import com.twill.redis.subscrible.subscriber.MessageListenerAdapterFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisController {

    @Autowired
    RedisMessageListener redisMessageListener;

    @GetMapping("/redis")
    public Object redisTest() throws InvocationTargetException, IllegalAccessException {
        return IServiceReflectionUtils.invokeMethod("redisUtil", "get", "aaa");
    }

    @GetMapping("/addListener")
    public Object addListener() {
        MessageListenerAdapter receiveMessage = MessageListenerAdapterFactory.create(new MessageSubscriber());
        RedisMessageListenerHolder.getContainer().addMessageListener(receiveMessage,
                new PatternTopic(RedisChannelConstants.TOPIC_B));
        RedisSerializer redisSerializer = RedisConfigHolder.getRedisSerializer();
        RedisMessageListenerHolder.getContainer().setTopicSerializer(redisSerializer);
//        RedisMessageListenerHolder.getContainer().afterPropertiesSet();
        return "success";
    }
}
